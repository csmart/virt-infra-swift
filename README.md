# Multi-node Swift Cluster

This is an example inventory and set of playbooks for my `csmart.swift` OpenStack Swift Ansible role (https://github.com/csmart/ansible-role-swift).

The inventory is also designed to support my `csmart.virt_infra` Ansible role (https://github.com/csmart/ansible-role-virt-infra) for spinning up virtual infrastructure on KVM hosts.

Therefore, it is able to create a multi-node PACO Swift cluster (plus an `admin` box) using CentOS 8 Stream on a KVM host, from scratch.

The virtual infrastructure will include:

 - Outward network for proxy access
 - Cluster network
 - Replication network
 - Multiple SCSI disks for object
 - SSD/NVMe disk for account and container

If you already have existing Swift nodes, then you can simply modify the inventory to suit your configuration and skip the VM creation step.

<img src="swift-ansible.png" alt="Swift cluster with Ansible">

An [SVG demo is included](demo.svg), if you want to see it in action.

## Inventory

The inventory is broken out into multiple files under `./inventory` directory.

 - `kvmhost.yml` - defining the settings for KVM host(s)
 - `swift.yml` - defining the settings for Swift nodes

If you want to run a cluster across multiple KVM hosts, you will need to modify the inventory to use bridged networks (which must already exist on the KVM hosts) as all nodes will need to be able to communicate.

## Requirements

If creating VMs, the `csmart.virt_infra` Ansible role will also configure libvirt on the host for you.
Therefore, all that's needed is a Linux box that is able to run KVM (defaults to `localhost`) and has the CentOS Stream 8 cloud image (https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20210603.0.x86_64.qcow2).

### Supported operating systems

The KVM host(s) can be any major Linux distro, such as Fedora, CentOS, Debian, Ubuntu, openSUSE, etc.

For the Swift cluster, only CentOS 8 Stream is currently supported.

#### Switching from CentOS 8 to Stream

If you are using existing nodes that are CentOS 8, you can switch them to Stream like so.

```
sudo dnf swap centos-linux-repos centos-stream-repos
sudo dnf distro-sync
sudo reboot
```

### SELinux

SELinux will be set to `permissive` mode on the Swift nodes for now, `enforcing` will be supported later.

## Spin up infrastructure on KVM

First, clone this Git repo.

```
git clone https://github.com/csmart/virt-infra-swift.git
cd virt-infra-swift
```

Download the CentOS cloud image and copy in-place for libvirt.

```bash
wget https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20210603.0.x86_64.qcow2
sudo mkdir -p /var/lib/libvirt/images
sudo mv CentOS-Stream-GenericCloud-8-20210603.0.x86_64.qcow2 /var/lib/libvirt/images/
```

Spin up virtual machines using `csmart.virt_infra` Ansible role on a KVM host (defaults to `localhost`).
The script simply wraps the Ansible commands, however it also makes sure that Ansible is installed and that the required roles are present.

```
./scripts/create-nodes.sh
```

This is the same as running the playbook directly.

```bash
ansible-playbook -i ./inventory ./ansible/virt-infra.yml
```

## Deploy Swift cluster

Once you have some machines ready, we can use the same inventory to deploy a multi-node PACO Swift cluster.

If you did not use the `csmart.virt_infra` role to spin up VMs, you will need to modify the `swift.yml` inventory file (or create your own) to suit your configuration.

This script will wrap the `csmart.swift` Ansible role to bootstrap the Swift cluster.

```bash
./scripts/site.sh
```

This is the same as running the playbook directly.

```bash
ansible-playbook -i ./inventory ./ansible/site.yml
```

### Test Swift cluster

Once the cluster is built, we can perform a simple test to make sure Swift is working.
This script will simply create a container, upload a new temp file, download the object and clean up.

```bash
./scripts/test-swift.sh
```

## Modify the Swift cluster

As Ansible is idempotent, you can simply modify the inventory and re-run the same full `site.sh` script.

However, rather than running everything you can:

 - Run site playbook with tags to run specific tasks, or
 - Run task specific playbook

For example, if you have modified the Swift Proxy service, you can run the site playbook with tags.

```bash
./scripts/site.sh --tags proxy
```

This is the same as running the playbook directly.

```bash
ansible-playbook -i ./inventory ./ansible/site.yml --tags proxy
```

Or you can run the proxy specific playbook.

```bash
ansible-playbook -i ./inventory ./ansible/swift-proxy-configure.yml
```

## Destroy the virtual machines

We can completely destroy the virtual machines (and therefore the Swift cluster) using the same playbook.
This script wraps the `csmart.virt_infra` Ansible role, but sets the state of the Swift VMs to `undefined`, so that they are deleted and cleaned up.


```bash
./scripts/destroy-nodes.sh
```

This is the same as running the playbook directly and passing in an extra argument to set the state to undefined.

```bash
ansible-playbook -i ./inventory ./ansible/virt-infra.yml -e virt_infra_state=undefined
```
